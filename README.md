# dotnet-build-reference-packages

The dotnet-build-reference-packages package contains reference assemblies needed to build .NET Core.

This package is not meant to be used by end-users.

For developers, you can easily update this package using:

    ./update-release <new-upstream-git-sha>
